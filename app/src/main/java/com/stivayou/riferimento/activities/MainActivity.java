package com.stivayou.riferimento.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.stivayou.riferimento.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.button_qualifiers).setOnClickListener(new ButtonClickListener( QualifiersActivity.class ));

        findViewById(R.id.button_feedback).setOnClickListener(new ButtonClickListener( FeedbackActivity.class ));

        findViewById(R.id.button_patch).setOnClickListener(new ButtonClickListener( PatchActivity.class ));

        findViewById(R.id.button_selector).setOnClickListener(new ButtonClickListener( SelectorActivity.class ));

        findViewById(R.id.button_constraint).setOnClickListener(new ButtonClickListener( ConstraintActivity.class ));

        findViewById(R.id.button_async).setOnClickListener(new ButtonClickListener( AsyncActivity.class ));

        findViewById(R.id.button_layout).setOnClickListener(new ButtonClickListener( JavaLayoutActivity.class ));

        findViewById(R.id.button_json).setOnClickListener(new ButtonClickListener( JsonActivity.class ));

        findViewById(R.id.button_recycler).setOnClickListener(new ButtonClickListener( RecyclerActivity.class ));

        findViewById(R.id.button_instance).setOnClickListener(new ButtonClickListener( InstanceActivity.class ));

        findViewById(R.id.button_recycler_multi).setOnClickListener(new ButtonClickListener( CountryLetterActivity.class ));

        findViewById(R.id.button_recycler_notify).setOnClickListener(new ButtonClickListener( RecyclerNotifyActivity.class ));

        findViewById(R.id.button_recycler_click).setOnClickListener(new ButtonClickListener( RecyclerClickActivity.class ));



    }

    class ButtonClickListener implements View.OnClickListener {

        Class clazz;

        public ButtonClickListener(Class clazz){
            this.clazz = clazz;
        }

        @Override
        public void onClick(View v) {
            startActivity( new Intent(MainActivity.this,clazz) );
        }
    }
}
