package com.stivayou.riferimento.activities;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.stivayou.riferimento.R;
import com.stivayou.riferimento.datagetter.DataGetter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JsonActivity extends AppCompatActivity {

    LinearLayout container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_json);

        container = findViewById(R.id.json_container);

        DataGetter getter = new DataGetter("https://api.openaq.org/v1/countries");
        getter.getData(new DataGetter.ResultCallback() {
            @Override
            public void onError(int errorCode) {
                Toast.makeText(JsonActivity.this,"Errore",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(String result) {

                try {
                    JSONObject obj = new JSONObject(result);

                    JSONArray results = obj.getJSONArray("results");

                    LayoutInflater inflater = LayoutInflater.from(JsonActivity.this);

                    for( int i = 0; i < results.length(); i++ ){

                        //addRowToContainer(results, i);
                        //addTextViewsToContainer(results,i);

                        JSONObject country = results.getJSONObject(i);

                        View row = inflater.inflate(R.layout.item_row_json, container,false);

                        ((TextView) row.findViewById(R.id.item_row_json_name)).setText(country.getString("name"));

                        ((TextView) row.findViewById(R.id.item_row_json_code)).setText(country.getString("code"));

                        ((TextView) row.findViewById(R.id.item_row_json_locations)).setText(country.getString("locations"));

                        container.addView(row);

                    }




                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

    }

    public void addRowToContainer(JSONArray results, int i) throws JSONException {
        TextView text = new TextView(JsonActivity.this);
        text.setText( results.getString(i) );
        text.setGravity(Gravity.CENTER_HORIZONTAL);

        if( i % 2 == 0 ){
            text.setBackgroundColor(Color.CYAN);
        }

        container.addView(text);
    }

    public void addTextViewsToContainer(JSONArray results, int i) throws JSONException {
        JSONObject country = results.getJSONObject(i);

        LinearLayout linear = new LinearLayout(JsonActivity.this);

        TextView textName = new TextView(JsonActivity.this);
        textName.setText( country.getString("name") );
        linear.addView( textName );

        TextView textCode = new TextView(JsonActivity.this);
        textCode.setText( country.getString("code") );
        textCode.setBackgroundColor(Color.GRAY);
        linear.addView( textCode );

        TextView textLocations = new TextView(JsonActivity.this);
        textLocations.setText( country.getString("locations") );
        linear.addView( textLocations );

        container.addView(linear);
    }
}
