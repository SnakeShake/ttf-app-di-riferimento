package com.stivayou.riferimento.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.stivayou.riferimento.R;
import com.stivayou.riferimento.adapters.CountryLetterAdapter;
import com.stivayou.riferimento.adapters.NotifyAdapter;
import com.stivayou.riferimento.datagetter.DataGetter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class RecyclerNotifyActivity extends AppCompatActivity {

    RecyclerView recycler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_notify);

        recycler = findViewById(R.id.recycler_notify_recycler);
        recycler.setLayoutManager( new LinearLayoutManager(this) );
        recycler.setHasFixedSize(true);

        DataGetter getter = new DataGetter( "https://api.openaq.org/v1/countries" );

        getter.getData(new DataGetter.ResultCallback() {
            @Override
            public void onError(int errorCode) {
                Toast.makeText(RecyclerNotifyActivity.this,"Errore!",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(String result) {

                try {

                    JSONObject total = new JSONObject( result );

                    JSONArray results = total.getJSONArray("results");

                    recycler.setAdapter( new NotifyAdapter( results ) );



                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });




        findViewById(R.id.recycler_notify_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if( recycler.getAdapter() != null )
                    ((NotifyAdapter) recycler.getAdapter()).addTen();

            }
        });




    }
}
