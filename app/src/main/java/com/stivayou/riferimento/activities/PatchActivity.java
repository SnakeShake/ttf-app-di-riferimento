package com.stivayou.riferimento.activities;

import android.graphics.Color;
import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.stivayou.riferimento.R;

public class PatchActivity extends AppCompatActivity {

    TextView text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patch);



        text = (TextView) findViewById(R.id.patch_text);

        findViewById(R.id.patch_gradient).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                text.setBackgroundResource(R.drawable.main_background);
                text.setTextColor(Color.BLACK);
                text.setPadding(0,0,0,0);

            }
        });

        findViewById(R.id.patch_flat).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                text.setBackgroundResource(R.drawable.earthrise_flat);
                text.setTextColor(Color.WHITE);
                text.setPadding(0,0,0,0);

            }
        });

        findViewById(R.id.patch_patch).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                text.setBackgroundResource(R.drawable.earthrise);
                text.setTextColor(Color.WHITE);

            }
        });

    }
}
