package com.stivayou.riferimento.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.stivayou.riferimento.R;
import com.stivayou.riferimento.datagetter.DataGetter;

public class AsyncActivity extends AppCompatActivity {

    TextView text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_async);

        text = findViewById(R.id.async_text);

        DataGetter getter = new DataGetter("https://api.openaq.org/v1/countries");
        getter.getData(new DataGetter.ResultCallback() {
            @Override
            public void onError(int errorCode) {
                Toast.makeText(AsyncActivity.this, "Errore!",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(String result) {
                text.setText(result);
            }
        });

    }
}
