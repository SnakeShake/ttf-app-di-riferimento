package com.stivayou.riferimento.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.stivayou.riferimento.R;

public class ConstraintActivity extends AppCompatActivity {

    TextView textLeft, textRight;
    Button buttonLeft, buttonRight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_constraint);

        textLeft = findViewById(R.id.constraint_text_left);
        textRight = findViewById(R.id.constraint_text_right);
        buttonLeft = findViewById(R.id.constraint_button_left);
        buttonRight = findViewById(R.id.constraint_button_right);

        buttonLeft.setOnClickListener( new TextClickListener() );
        buttonRight.setOnClickListener( new TextClickListener() );

    }

    class TextClickListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {

            if( view.getId() == R.id.constraint_button_left ){

                if( textLeft.getText().equals( getResources().getString(R.string.constraint_three) ) ){
                    textLeft.setText(R.string.constraint_six);
                    buttonLeft.setText(R.string.constraint_fewer);
                }
                else{
                    textLeft.setText(R.string.constraint_three);
                    buttonLeft.setText(R.string.constraint_more);
                }

            }
            else{

                if( textRight.getText().equals( getResources().getString(R.string.constraint_three) ) ){
                    textRight.setText(R.string.constraint_six);
                    buttonRight.setText(R.string.constraint_fewer);
                }
                else{
                    textRight.setText(R.string.constraint_three);
                    buttonRight.setText(R.string.constraint_more);
                }

            }

        }

    }
}
