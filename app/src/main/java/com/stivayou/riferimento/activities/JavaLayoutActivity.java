package com.stivayou.riferimento.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.stivayou.riferimento.R;

public class JavaLayoutActivity extends AppCompatActivity {

    LinearLayout container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_java_layout);

        container = findViewById(R.id.layout_java_container);

        findViewById(R.id.button_add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                TextView text = new TextView( JavaLayoutActivity.this );
                text.setText(R.string.app_name);
                text.setGravity(Gravity.CENTER_HORIZONTAL);
                container.addView(text);

            }
        });

        findViewById(R.id.button_remove).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if( container.getChildCount() == 0 )
                    return;

                container.removeViewAt( container.getChildCount() - 1 );

            }
        });

    }
}
