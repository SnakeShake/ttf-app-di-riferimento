package com.stivayou.riferimento.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.stivayou.riferimento.R;
import com.stivayou.riferimento.adapters.CountryAdapter;
import com.stivayou.riferimento.datagetter.DataGetter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class InstanceActivity extends AppCompatActivity {

    static final String SAVE_DATA = "save_data";
    static final String SAVE_TS = "save_ts";

    RecyclerView recycler;

    SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler);

        prefs = getSharedPreferences("test", Context.MODE_PRIVATE);

        /*
        if( savedInstanceState != null && savedInstanceState.containsKey(SAVE_DATA) ){
            oldData = savedInstanceState.getString(SAVE_DATA);
        }
        */

        recycler = findViewById(R.id.recycler_recycler);
        recycler.setLayoutManager( new LinearLayoutManager(this) );

    }

    @Override
    protected void onResume() {
        super.onResume();

        boolean isFresh = false;

        if( prefs.contains(SAVE_TS) ){

            if( System.currentTimeMillis() - (5 * 60 * 1000) < prefs.getLong(SAVE_TS,0L) ){
                isFresh = true;
            }

        }

        if( !prefs.contains(SAVE_DATA) || !isFresh ){

            Toast.makeText(this, "Prendo dati da internet",Toast.LENGTH_LONG).show();

            DataGetter getter = new DataGetter("https://api.openaq.org/v1/countries");

            getter.getData(new DataGetter.ResultCallback() {
                @Override
                public void onError(int errorCode) {
                    Toast.makeText(InstanceActivity.this,"Errore",Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onSuccess(String result) {

                    try {
                        JSONObject obj = new JSONObject(result);

                        JSONArray results = obj.getJSONArray("results");

                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putString( SAVE_DATA, results.toString() );
                        editor.putLong( SAVE_TS, System.currentTimeMillis() );
                        editor.commit();

                        recycler.setAdapter( new CountryAdapter( results ) );


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });
        }
        else{

            Toast.makeText(this, "Prendo dati salvati",Toast.LENGTH_LONG).show();

            try {
                JSONArray results = new JSONArray( prefs.getString(SAVE_DATA,"") );

                recycler.setAdapter( new CountryAdapter( results ) );

            } catch (JSONException e) {
                e.printStackTrace();
            }



        }

    }

    /*
    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString( SAVE_DATA, oldData );
        super.onSaveInstanceState(outState);

    }
    */
}
