package com.stivayou.riferimento.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.stivayou.riferimento.R;
import com.stivayou.riferimento.adapters.CountryAdapter;
import com.stivayou.riferimento.datagetter.DataGetter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class RecyclerActivity extends AppCompatActivity {

    RecyclerView recycler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler);

        recycler = findViewById(R.id.recycler_recycler);
        recycler.setLayoutManager( new LinearLayoutManager(this) );

        DataGetter getter = new DataGetter("https://api.openaq.org/v1/countries");

        getter.getData(new DataGetter.ResultCallback() {
            @Override
            public void onError(int errorCode) {
                Toast.makeText(RecyclerActivity.this,"Errore",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(String result) {

                try {
                    JSONObject obj = new JSONObject(result);

                    JSONArray results = obj.getJSONArray("results");

                    recycler.setAdapter( new CountryAdapter( results ) );


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

    }
}
