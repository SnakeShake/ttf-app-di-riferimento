package com.stivayou.riferimento.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.stivayou.riferimento.R;
import com.stivayou.riferimento.adapters.CountryLetterAdapter;
import com.stivayou.riferimento.datagetter.DataGetter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class CountryLetterActivity extends AppCompatActivity {

    RecyclerView recycler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country_letter);

        recycler = findViewById(R.id.CountryLetterRecycler);
        recycler.setLayoutManager( new LinearLayoutManager(this) );
        recycler.setHasFixedSize(true);

        DataGetter getter = new DataGetter( "https://api.openaq.org/v1/countries" );

        getter.getData(new DataGetter.ResultCallback() {
            @Override
            public void onError(int errorCode) {
                Toast.makeText(CountryLetterActivity.this,"Errore!",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(String result) {

                try {

                    JSONObject total = new JSONObject( result );

                    JSONArray results = total.getJSONArray("results");

                    recycler.setAdapter( new CountryLetterAdapter( results ) );



                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });


    }
}
