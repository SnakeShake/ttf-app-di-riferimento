package com.stivayou.riferimento.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.stivayou.riferimento.R;
import com.stivayou.riferimento.adapters.CountryLetterAdapter;
import com.stivayou.riferimento.adapters.RecyclerClickAdapter;
import com.stivayou.riferimento.datagetter.DataGetter;
import com.stivayou.riferimento.objects.Country;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class RecyclerClickActivity extends AppCompatActivity {

    RecyclerView recycler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country_letter);

        recycler = findViewById(R.id.CountryLetterRecycler);
        recycler.setLayoutManager( new LinearLayoutManager(this) );
        recycler.setHasFixedSize(true);

        DataGetter getter = new DataGetter( "https://api.openaq.org/v1/countries" );

        getter.getData(new DataGetter.ResultCallback() {
            @Override
            public void onError(int errorCode) {
                Toast.makeText(RecyclerClickActivity.this,"Errore!",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(String result) {

                try {

                    JSONObject total = new JSONObject( result );

                    JSONArray results = total.getJSONArray("results");

                    recycler.setAdapter( new RecyclerClickAdapter(results, new RecyclerClickAdapter.OnCountryClickListener() {
                        @Override
                        public void onClick(Country country) {

                            // Launch new Activity
                            Intent locationIntent = new Intent(RecyclerClickActivity.this, LocationActivity.class);
                            locationIntent.putExtra( "countryCode", country.code );
                            startActivity( locationIntent );

                        }
                    }) );



                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });


    }
}
