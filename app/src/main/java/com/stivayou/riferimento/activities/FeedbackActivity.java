package com.stivayou.riferimento.activities;

import android.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.stivayou.riferimento.R;

public class FeedbackActivity extends AppCompatActivity {

    EditText edit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);

        edit = (EditText) findViewById(R.id.feedback_edit);

        findViewById(R.id.feedback_toast).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(FeedbackActivity.this,"Ciaooooo",Toast.LENGTH_SHORT).show();
            }
        });

        findViewById(R.id.feedback_alert).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(FeedbackActivity.this);
                AlertDialog dialog = builder.setTitle("Titolo")
                        .setMessage("Messaggio").setNegativeButton("Annulla",null).create();
                dialog.show();

            }
        });

        findViewById(R.id.feedback_check).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if( edit.getError() == null ){
                    edit.setError("Errore!");
                }
                else{
                    edit.setError(null);
                }



            }
        });

    }
}
