package com.stivayou.riferimento.datagetter;

import android.os.AsyncTask;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

/**
 * Created by stokm on 19/03/2018.
 */

public class DataGetter {

    String target;

    /**
     * Default constructor
     * @param target URL in string form
     */
    public DataGetter(String target){
        this.target = target;
    }

    /**
     * Calls previously defined URL
     */
    public void getData( ResultCallback callback ){

        new AsyncGetter( callback ).execute();

    }

    class AsyncGetter extends AsyncTask<Void,Void,Integer>{

        String result = "";
        ResultCallback callback;

        public AsyncGetter(ResultCallback callback){
            this.callback = callback;
        }

        @Override
        protected Integer doInBackground(Void... voids) {

            HttpURLConnection connection = null;

            try {
                URL targetUrl = new URL(target);
                connection = (HttpURLConnection) targetUrl.openConnection();

                if( connection.getResponseCode() != HttpURLConnection.HTTP_OK ){
                    connection.disconnect();
                    return 2;
                }
                else{
                    InputStream in = connection.getInputStream();

                    Scanner scanner = new Scanner(in);
                    scanner.useDelimiter("\\A");

                    if( scanner.hasNext() ){
                        result = scanner.next();
                    }

                }
            } catch (java.io.IOException e) {
                e.printStackTrace();
                return 1;
            } finally {
                if( connection != null )
                    connection.disconnect();

                return 0;
            }

        }

        @Override
        protected void onPostExecute(Integer integer) {

            if( integer != 0 ){
                callback.onError(integer);
            }
            else{
                callback.onSuccess(result);
            }

        }
    }



    public interface ResultCallback{
        void onError( int errorCode );
        void onSuccess( String result );
    }

}
