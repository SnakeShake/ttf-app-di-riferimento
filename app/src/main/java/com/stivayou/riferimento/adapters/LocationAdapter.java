package com.stivayou.riferimento.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.stivayou.riferimento.R;
import com.stivayou.riferimento.objects.Country;
import com.stivayou.riferimento.objects.Location;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by stokm on 05/04/2018.
 */

public class LocationAdapter extends RecyclerView.Adapter {

    ArrayList<Location> locations = new ArrayList<>();

    public LocationAdapter(JSONArray locationsJSON ) {

        int lLength = locationsJSON.length();
        for( int i = 0; i < lLength; i++ ){

            try {
                JSONObject locationJSON = locationsJSON.getJSONObject(i);

                Location location = new Location();
                location.name = locationJSON.getString("location");

                locations.add( location );


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from( parent.getContext() );

        View row = inflater.inflate( R.layout.item_row_json, parent,false );

        CountryViewHolder holder = new CountryViewHolder( row );

        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        CountryViewHolder cHolder = (CountryViewHolder) holder;

        cHolder.textName.setText( locations.get(position).name );


    }

    @Override
    public int getItemCount() {
        return locations.size();
    }



    class CountryViewHolder extends RecyclerView.ViewHolder {

        public TextView textName;

        public CountryViewHolder(View itemView) {
            super(itemView);

            textName = itemView.findViewById(R.id.item_row_json_name);


        }
    }

}
