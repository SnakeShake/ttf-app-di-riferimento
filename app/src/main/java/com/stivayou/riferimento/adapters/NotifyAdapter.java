package com.stivayou.riferimento.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.stivayou.riferimento.R;
import com.stivayou.riferimento.objects.Country;
import com.stivayou.riferimento.objects.Letter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by stokm on 05/04/2018.
 */

public class NotifyAdapter extends RecyclerView.Adapter {

    final static int TYPE_LETTER = 0;
    final static int TYPE_COUNTRY = 1;

    int positionBelgium = -1;

    //ArrayList<Country> locations = new ArrayList<>();
    ArrayList<Object> countriesAndLetters = new ArrayList<>();

    public NotifyAdapter(JSONArray countriesJSON ) {

        String lastLetter = "ciao";

        int cLength = countriesJSON.length();
        for( int i = 0; i < cLength; i++ ){

            try {
                JSONObject countryJSON = countriesJSON.getJSONObject(i);

                String countryName = countryJSON.getString("name");

                if( !countryName.startsWith( lastLetter ) ){
                    Letter letter = new Letter();
                    letter.indexLetter = String.valueOf( countryName.charAt(0) );

                    // add letter to object list
                    countriesAndLetters.add( letter );

                    // update flag
                    lastLetter = letter.indexLetter;

                }


                Country country = new Country();
                country.name = countryName;
                country.code = countryJSON.getString("code");
                country.locations = countryJSON.getInt("locations");

                countriesAndLetters.add( country );


            } catch (JSONException e) {
                e.printStackTrace();
            }


        }

    }

    @Override
    public int getItemViewType(int position) {

        if( countriesAndLetters.get(position) instanceof Letter ){
            return TYPE_LETTER;
        }
        else{
            return TYPE_COUNTRY;
        }

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from( parent.getContext() );

        if( viewType == TYPE_LETTER ){

            View row = inflater.inflate( R.layout.item_index_letter, parent,false );

            LetterViewHolder holder = new LetterViewHolder( row );

            return holder;

        }
        else{
            View row = inflater.inflate( R.layout.item_row_json, parent,false );

            CountryViewHolder holder = new CountryViewHolder( row );

            return holder;
        }


    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if( holder instanceof LetterViewHolder ){

            LetterViewHolder lHolder = (LetterViewHolder) holder;

            lHolder.indexText.setText( ( (Letter) countriesAndLetters.get( position ) ).indexLetter );

        }
        else{

            CountryViewHolder cHolder = (CountryViewHolder) holder;

            Country country = (Country) countriesAndLetters.get( position );

            cHolder.textName.setText( country.name );
            cHolder.textCode.setText( country.code );
            cHolder.textLocations.setText( country.locations + "" );

        }






    }

    @Override
    public int getItemCount() {
        return countriesAndLetters.size();
    }


    public void addTen(){

        if( positionBelgium != -1 ){

            ((Country) countriesAndLetters.get( positionBelgium )).locations += 10;
            notifyItemChanged(positionBelgium);

        }
        else{

            int cLength = countriesAndLetters.size();
            for( int i = 0; i < cLength; i++ ){

                if( countriesAndLetters.get(i) instanceof Country ){

                    Country country = (Country) countriesAndLetters.get(i);

                    if( country.name.equals("Belgium") ){

                        positionBelgium = i;

                        country.locations += 10;
                        notifyItemChanged(i);

                        break;

                    }

                }

            }

        }




    }


    class CountryViewHolder extends RecyclerView.ViewHolder {

        public TextView textName, textCode, textLocations;

        public CountryViewHolder(View itemView) {
            super(itemView);

            textName = itemView.findViewById(R.id.item_row_json_name);
            textCode = itemView.findViewById(R.id.item_row_json_code);
            textLocations = itemView.findViewById(R.id.item_row_json_locations);


        }
    }

    class LetterViewHolder extends RecyclerView.ViewHolder {

        TextView indexText;

        public LetterViewHolder(View itemView) {
            super(itemView);

            indexText = (TextView) itemView;
        }

    }

}
