package com.stivayou.riferimento.adapters;

import android.app.ActionBar;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.stivayou.riferimento.R;
import com.stivayou.riferimento.objects.Country;
import com.stivayou.riferimento.objects.CountryVisibility;
import com.stivayou.riferimento.objects.Letter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by stokm on 05/04/2018.
 */

public class RecyclerClickAdapter extends RecyclerView.Adapter {

    final static int TYPE_LETTER = 0;
    final static int TYPE_COUNTRY = 1;

    //ArrayList<Country> locations = new ArrayList<>();
    ArrayList<Object> countriesAndLetters = new ArrayList<>();

    OnCountryClickListener cListener = null;

    public RecyclerClickAdapter(JSONArray countriesJSON, @NonNull OnCountryClickListener listener ) {

        cListener = listener;

        String lastLetter = "ciao";

        int cLength = countriesJSON.length();
        for( int i = 0; i < cLength; i++ ){

            try {
                JSONObject countryJSON = countriesJSON.getJSONObject(i);

                String countryName = countryJSON.getString("name");

                if( !countryName.startsWith( lastLetter ) ){
                    Letter letter = new Letter();
                    letter.indexLetter = String.valueOf( countryName.charAt(0) );

                    // add letter to object list
                    countriesAndLetters.add( letter );

                    // update flag
                    lastLetter = letter.indexLetter;

                }


                CountryVisibility country = new CountryVisibility();
                country.name = countryName;
                country.code = countryJSON.getString("code");
                country.locations = countryJSON.getInt("locations");

                countriesAndLetters.add( country );


            } catch (JSONException e) {
                e.printStackTrace();
            }


        }

    }

    @Override
    public int getItemViewType(int position) {

        if( countriesAndLetters.get(position) instanceof Letter ){
            return TYPE_LETTER;
        }
        else{
            return TYPE_COUNTRY;
        }

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from( parent.getContext() );

        if( viewType == TYPE_LETTER ){

            View row = inflater.inflate( R.layout.item_index_letter, parent,false );

            LetterViewHolder holder = new LetterViewHolder( row );

            return holder;

        }
        else{
            View row = inflater.inflate( R.layout.item_row_json, parent,false );

            CountryViewHolder holder = new CountryViewHolder( row );

            return holder;
        }


    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if( holder instanceof LetterViewHolder ){

            LetterViewHolder lHolder = (LetterViewHolder) holder;

            lHolder.indexText.setText( ( (Letter) countriesAndLetters.get( position ) ).indexLetter );

        }
        else{

            CountryViewHolder cHolder = (CountryViewHolder) holder;

            CountryVisibility country = (CountryVisibility) countriesAndLetters.get( position );

            cHolder.textName.setText( country.name );
            cHolder.textCode.setText( country.code );
            cHolder.textLocations.setText( country.locations + "" );

            if( country.visible ){
                cHolder.container.setVisibility(View.VISIBLE);
                cHolder.container.setLayoutParams( new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT,RecyclerView.LayoutParams.WRAP_CONTENT));
            }
            else{
                cHolder.container.setVisibility(View.GONE);
                cHolder.container.setLayoutParams( new RecyclerView.LayoutParams(0,0));
            }

        }






    }

    @Override
    public int getItemCount() {
        return countriesAndLetters.size();
    }



    class CountryViewHolder extends RecyclerView.ViewHolder {

        View container;
        public TextView textName, textCode, textLocations;

        public CountryViewHolder(View itemView) {
            super(itemView);

            container = itemView;
            textName = itemView.findViewById(R.id.item_row_json_name);
            textCode = itemView.findViewById(R.id.item_row_json_code);
            textLocations = itemView.findViewById(R.id.item_row_json_locations);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    //String name = ((Country) countriesAndLetters.get(getAdapterPosition())).name;

                    cListener.onClick( ((Country) countriesAndLetters.get(getAdapterPosition())) );


                }
            });

        }
    }

    class LetterViewHolder extends RecyclerView.ViewHolder {

        TextView indexText;

        public LetterViewHolder(View itemView) {
            super(itemView);

            indexText = (TextView) itemView;

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    int letterPosition = getAdapterPosition(), cLength = countriesAndLetters.size();
                    for( int i = letterPosition + 1; i < cLength; i++ ){

                        if( countriesAndLetters.get(i) instanceof Letter )
                            break;

                        CountryVisibility country = (CountryVisibility) countriesAndLetters.get(i);
                        country.visible = !country.visible;
                        notifyItemChanged(i);

                    }



                }
            });
        }

    }

    public interface OnCountryClickListener {
        void onClick( Country country );
    }

}
