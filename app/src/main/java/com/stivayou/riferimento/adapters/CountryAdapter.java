package com.stivayou.riferimento.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.stivayou.riferimento.R;
import com.stivayou.riferimento.objects.Country;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by stokm on 05/04/2018.
 */

public class CountryAdapter extends RecyclerView.Adapter {

    ArrayList<Country> countries = new ArrayList<>();

    public CountryAdapter( JSONArray countriesJSON ) {

        int cLength = countriesJSON.length();
        for( int i = 0; i < cLength; i++ ){

            try {
                JSONObject countryJSON = countriesJSON.getJSONObject(i);

                Country country = new Country();
                country.name = countryJSON.getString("name");
                country.code = countryJSON.getString("code");
                country.locations = countryJSON.getInt("locations");

                countries.add( country );


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from( parent.getContext() );

        View row = inflater.inflate( R.layout.item_row_json, parent,false );

        CountryViewHolder holder = new CountryViewHolder( row );

        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        CountryViewHolder cHolder = (CountryViewHolder) holder;

        cHolder.textName.setText( countries.get(position).name );
        cHolder.textCode.setText( countries.get(position).code );
        cHolder.textLocations.setText( countries.get(position).locations + "" );


    }

    @Override
    public int getItemCount() {
        return countries.size();
    }



    class CountryViewHolder extends RecyclerView.ViewHolder {

        public TextView textName, textCode, textLocations;

        public CountryViewHolder(View itemView) {
            super(itemView);

            textName = itemView.findViewById(R.id.item_row_json_name);
            textCode = itemView.findViewById(R.id.item_row_json_code);
            textLocations = itemView.findViewById(R.id.item_row_json_locations);


        }
    }

}
